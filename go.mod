module token-balance-checker

go 1.15

require (
	github.com/HAL-xyz/ethrpc v1.0.2
	github.com/HAL-xyz/web3-multicall-go v0.0.19
	github.com/ethereum/go-ethereum v1.9.25
	github.com/stretchr/testify v1.7.0
	gorm.io/driver/sqlite v1.2.6
	gorm.io/gorm v1.22.4
)
