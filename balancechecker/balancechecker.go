package balancechecker

import (
	"fmt"
	"math/big"
	"strings"
	"sync"

	"github.com/HAL-xyz/web3-multicall-go/multicall"
	"github.com/ethereum/go-ethereum/common"
)

type Persistence interface {
	InsertBalances(balances []Balance) error
	GetAllBalances() ([]Balance, error)
}

type EthRPCClient interface {
	MakeEthRpcCall(cntAddress, data string, blockNumber int) (string, error)
	LastBlockHex() (string, error)
}

type Multicaller interface {
	Call(calls multicall.ViewCalls, lastBlockHex string) (*multicall.Result, error)
}

type Trigger struct {
	TriggerName     string `json:"TriggerName"`
	UserAddress     string `json:"UserAddress"`
	ContractAddress string `json:"ContractAddress"`
	Method          string `json:"Method"`
}

func (t *Trigger) Validate() error {
	if t.TriggerName == "" {
		return fmt.Errorf("attribute TriggerName is required")
	}

	if t.UserAddress == "" {
		return fmt.Errorf("attribute UserAddress is required")
	}

	if t.ContractAddress == "" {
		return fmt.Errorf("attribute ContractAddress is required")
	}

	if t.Method == "" {
		return fmt.Errorf("attribute Method is required")
	}

	if !strings.Contains(t.Method, "balanceOf") {
		return fmt.Errorf("attribute Method not implemented: %s", t.Method)
	}

	return nil
}

type Balance struct {
	ID              int    `json:"id" gorm:"column:id"`
	TriggerName     string `json:"triggerName" gorm:"column:trigger_name"`
	UserAddress     string `json:"userAddress" gorm:"column:user_address"`
	ContractAddress string `json:"contractAddress" gorm:"column:contract_address"`
	Balance         string `json:"balance" gorm:"column:balance"`
}

type balanceChecker struct {
	ethrpccli      EthRPCClient
	db             Persistence
	multicall      Multicaller
	nrOfConsumers  int
	callsBatchSize int
}

func New(ethrpccli EthRPCClient, db Persistence, mc Multicaller, nrOfConsumers, batchSize int) *balanceChecker {
	return &balanceChecker{
		ethrpccli:      ethrpccli,
		db:             db,
		multicall:      mc,
		nrOfConsumers:  nrOfConsumers,
		callsBatchSize: batchSize,
	}
}

func FilterUniqueTriggers(triggers []*Trigger) []*Trigger {
	contractAndUserAddrToTrigger := make(map[string]*Trigger)
	for _, t := range triggers {
		if err := t.Validate(); err != nil {
			fmt.Printf("invalid trigger\n: %v", err)
			continue
		}

		contractAndUserAddrToTrigger[genID(t.ContractAddress, t.UserAddress)] = t
	}

	var filteredTriggers []*Trigger
	for _, t := range contractAndUserAddrToTrigger {
		filteredTriggers = append(filteredTriggers, t)
	}

	return filteredTriggers
}

func (bc *balanceChecker) CheckBalances(triggersInput []*Trigger) error {
	triggers := FilterUniqueTriggers(triggersInput)

	if len(triggers) == 0 {
		return fmt.Errorf("no valid trigger was found in the input")
	}

	errChan := make(chan error)
	done := make(chan bool)

	balancesChan := bc.Producer(triggers, errChan)

	go bc.Consumer(balancesChan, errChan, done)

	for {
		select {
		case <-done:
			fmt.Println("Token balances checker is done")

			balancesSaved, err := bc.db.GetAllBalances()
			if err != nil {
				return fmt.Errorf("failed to get all balances from db: %v", err)
			}

			fmt.Println("Balances saved in db:")
			for _, b := range balancesSaved {
				fmt.Printf("Balance: %+v\n", b)
			}

			return nil
		case err := <-errChan:
			fmt.Printf("Error: %v\n", err)
			return err
		}
	}

}

func (bc *balanceChecker) Producer(triggersInput []*Trigger, errChan chan error) chan []Balance {
	balancesChan := make(chan []Balance, bc.nrOfConsumers)
	var wg sync.WaitGroup

	var triggersBatched [][]*Trigger
	batch(len(triggersInput), bc.callsBatchSize, func(start, end int) {
		triggersBatched = append(triggersBatched, triggersInput[start:end+1])
	})

	for _, triggers := range triggersBatched {
		wg.Add(1)

		go func(triggers []*Trigger, wg *sync.WaitGroup) {
			balances, err := bc.GetBalances(triggers)
			if err != nil {
				errChan <- err
			}

			balancesChan <- balances
			wg.Done()

		}(triggers, &wg)
	}

	go func() {
		wg.Wait()
		close(balancesChan)
	}()

	return balancesChan

}

func (bc *balanceChecker) Consumer(balancesChan chan []Balance, errChan chan error, done chan bool) {
	var balances []Balance
	for b := range balancesChan {
		balances = append(balances, b...)
	}

	if err := bc.db.InsertBalances(balances); err != nil {
		fmt.Printf("failed to save to db: %v", err)
		errChan <- err
	}

	done <- true
}

func (bc *balanceChecker) CreateBalanceOfCall(t *Trigger) (multicall.ViewCall, error) {
	return multicall.NewViewCall(genID(t.ContractAddress, t.UserAddress), t.ContractAddress,
		t.Method, []interface{}{common.HexToAddress(t.UserAddress)}), nil
}

func (bc *balanceChecker) callMulticall(calls multicall.ViewCalls) (*multicall.Result, error) {
	lastBlockHex, err := bc.ethrpccli.LastBlockHex()
	if err != nil {
		return nil, fmt.Errorf("failed to get lastblock hex: %v", err)
	}

	return bc.multicall.Call(calls, lastBlockHex)
}

func (bc *balanceChecker) getBalances(res *multicall.Result, triggers []*Trigger) ([]Balance, error) {
	var balances []Balance
	for _, t := range triggers {
		callRes, ok := res.Calls[genID(t.ContractAddress, t.UserAddress)]
		if !ok {
			continue
		}

		if len(callRes.Decoded) > 0 {
			balanceInterface := callRes.Decoded[0]
			if balanceBigInt, ok := balanceInterface.(*big.Int); ok {
				b := Balance{
					TriggerName:     t.TriggerName,
					UserAddress:     t.UserAddress,
					ContractAddress: t.ContractAddress,
					Balance:         balanceBigInt.String(),
				}

				balances = append(balances, b)
			}
		}
	}

	return balances, nil
}

func (bc *balanceChecker) GetBalances(triggers []*Trigger) ([]Balance, error) {

	var calls multicall.ViewCalls
	for _, t := range triggers {
		c, err := bc.CreateBalanceOfCall(t)
		if err != nil {
			return nil, fmt.Errorf("failed to createBalanceOfCall: %v", err)
		}
		calls = append(calls, c)
	}

	res, err := bc.callMulticall(calls)
	if err != nil {
		return nil, err
	}

	return bc.getBalances(res, triggers)
}

func batch(count, batchSize int, eachFn func(start, end int)) {
	i := 0
	for i < count {
		end := i + batchSize - 1
		if end > count-1 {
			// passed end, so set to end item
			end = count - 1
		}
		eachFn(i, end)

		i = end + 1
	}
}

func genID(contractAddress, userAddress string) string {
	return contractAddress + userAddress
}
