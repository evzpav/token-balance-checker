package balancechecker

import (
	"fmt"
	"math/big"
	"testing"

	"github.com/HAL-xyz/web3-multicall-go/multicall"
	"github.com/stretchr/testify/assert"
)

type multicallMock struct {
	CallInvokedCount int
	CallFn           func(calls multicall.ViewCalls, lastBlockHex string) (*multicall.Result, error)
}

func (mcm *multicallMock) Call(calls multicall.ViewCalls, lastBlockHex string) (*multicall.Result, error) {
	mcm.CallInvokedCount++
	return mcm.CallFn(calls, lastBlockHex)
}

type ethrpccliMock struct {
	MakeEthRpcCallFn func(cntAddress, data string, blockNumber int) (string, error)
	LastBlockHexFn   func() (string, error)
}

func (ercm *ethrpccliMock) MakeEthRpcCall(cntAddress, data string, blockNumber int) (string, error) {
	return ercm.MakeEthRpcCallFn(cntAddress, data, blockNumber)
}

func (ercm *ethrpccliMock) LastBlockHex() (string, error) {
	return ercm.LastBlockHexFn()
}

type sqliteMock struct {
	InsertBalancesFn func(balances []Balance) error
	GetAllBalancesFn func() ([]Balance, error)
}

func (slm *sqliteMock) InsertBalances(balances []Balance) error {
	return slm.InsertBalancesFn(balances)
}

func (slm *sqliteMock) GetAllBalances() ([]Balance, error) {
	return slm.GetAllBalancesFn()
}

func TestFilterUniqueTriggers(t *testing.T) {

	t.Run("duplicate triggers", func(t *testing.T) {
		triggers := []*Trigger{
			{
				TriggerName:     "BAT",
				UserAddress:     "0x0000000000000000000000000000000000000000",
				ContractAddress: "0x0d8775f648430679a709e98d2b0cb6250d2887ef",
				Method:          "balanceOf(address)(uint256)",
			},
			{
				TriggerName:     "BAT",
				UserAddress:     "0x0000000000000000000000000000000000000000",
				ContractAddress: "0x0d8775f648430679a709e98d2b0cb6250d2887ef",
				Method:          "balanceOf(address)(uint256)",
			},
		}

		filteredTriggers := FilterUniqueTriggers(triggers)
		assert.Equal(t, 1, len(filteredTriggers))
	})

	t.Run("different user address", func(t *testing.T) {
		triggers := []*Trigger{
			{
				TriggerName:     "BAT",
				UserAddress:     "0x0000000000000000000000000000000000000000",
				ContractAddress: "0x0d8775f648430679a709e98d2b0cb6250d2887ef",
				Method:          "balanceOf(address)(uint256)",
			},
			{
				TriggerName:     "BAT",
				UserAddress:     "0x0000000000000000000000001111111111111111",
				ContractAddress: "0x0d8775f648430679a709e98d2b0cb6250d2887ef",
				Method:          "balanceOf(address)(uint256)",
			},
		}
		filteredTriggers := FilterUniqueTriggers(triggers)
		assert.Equal(t, 2, len(filteredTriggers))
	})

}

func TestGetBalances(t *testing.T) {
	triggers := []*Trigger{
		{
			TriggerName:     "BAT",
			UserAddress:     "0x0000000000000000000000000000000000000000",
			ContractAddress: "0x0d8775f648430679a709e98d2b0cb6250d2887ef",
			Method:          "balanceOf(address)(uint256)",
		},
		{
			TriggerName:     "ETHLAND",
			UserAddress:     "0x0000000000000000000000000000000000000000",
			ContractAddress: "0x80fB784B7eD66730e8b1DBd9820aFD29931aab03",
			Method:          "balanceOf(address)(uint256)",
		},
	}

	t.Run("GetAllBalances should return string balances", func(t *testing.T) {

		ethrpccliMock := ethrpccliMock{
			LastBlockHexFn: func() (string, error) {
				return "blockhex", nil
			},
		}

		multicallMock := multicallMock{
			CallFn: func(calls multicall.ViewCalls, lastBlockHex string) (*multicall.Result, error) {
				assert.Equal(t, "blockhex", lastBlockHex)

				return &multicall.Result{
					Calls: map[string]multicall.CallResult{
						"0x0d8775f648430679a709e98d2b0cb6250d2887ef0x0000000000000000000000000000000000000000": {
							Success: true,
							Decoded: []interface{}{big.NewInt(10000)},
						},
						"0x80fB784B7eD66730e8b1DBd9820aFD29931aab030x0000000000000000000000000000000000000000": {
							Success: true,
							Decoded: []interface{}{big.NewInt(20000)},
						},
					},
				}, nil
			},
		}

		balanceChecker := New(&ethrpccliMock, nil, &multicallMock, 1, 1)
		balances, err := balanceChecker.GetBalances(triggers)
		assert.Nil(t, err)
		assert.Equal(t, 2, len(balances))
		assert.Equal(t, "10000", balances[0].Balance)
		assert.Equal(t, "20000", balances[1].Balance)
		assert.Equal(t, 1, multicallMock.CallInvokedCount)
	})

}

func TestProducer(t *testing.T) {

	NewMulticallMock := func() *multicallMock {
		return &multicallMock{
			CallFn: func(calls multicall.ViewCalls, lastBlockHex string) (*multicall.Result, error) {
				assert.Equal(t, "blockhex", lastBlockHex)

				return &multicall.Result{
					Calls: map[string]multicall.CallResult{
						"0x0d8775f648430679a709e98d2b0cb6250d2887ef0x0000000000000000000000000000000000000000": {
							Success: true,
							Decoded: []interface{}{big.NewInt(10000)},
						},
						"0x80fB784B7eD66730e8b1DBd9820aFD29931aab030x0000000000000000000000000000000000000000": {
							Success: true,
							Decoded: []interface{}{big.NewInt(20000)},
						},
						"0xcb97e65f07da24d46bcdd078ebebd7c6e6e3d7500x0000000000000000000000000000000000000000": {
							Success: true,
							Decoded: []interface{}{big.NewInt(30000)},
						},
					},
				}, nil
			},
		}
	}

	triggers := []*Trigger{
		{
			TriggerName:     "BAT",
			UserAddress:     "0x0000000000000000000000000000000000000000",
			ContractAddress: "0x0d8775f648430679a709e98d2b0cb6250d2887ef",
			Method:          "balanceOf(address)(uint256)",
		},
		{
			TriggerName:     "ETHLAND",
			UserAddress:     "0x0000000000000000000000000000000000000000",
			ContractAddress: "0x80fB784B7eD66730e8b1DBd9820aFD29931aab03",
			Method:          "balanceOf(address)(uint256)",
		},
		{
			TriggerName:     "Bytom",
			UserAddress:     "0x0000000000000000000000000000000000000000",
			ContractAddress: "0xcb97e65f07da24d46bcdd078ebebd7c6e6e3d750",
			Method:          "balanceOf(address)(uint256)",
		},
	}

	ethrpccliMock := ethrpccliMock{
		LastBlockHexFn: func() (string, error) {
			return "blockhex", nil
		},
	}

	t.Run("Producer - batch size 3", func(t *testing.T) {
		batchSize := 3
		multMock := NewMulticallMock()
		balanceChecker := New(&ethrpccliMock, nil, multMock, 3, batchSize)
		errChan := make(chan error)

		balancesChan := balanceChecker.Producer(triggers, errChan)
		for balances := range balancesChan {
			assert.Len(t, balances, 3)
			assert.Equal(t, "10000", balances[0].Balance)
			assert.Equal(t, "20000", balances[1].Balance)
			assert.Equal(t, "30000", balances[2].Balance)
		}

		assert.Equal(t, 1, multMock.CallInvokedCount)
	})

	t.Run("Producer - batch size 2", func(t *testing.T) {
		batchSize := 2
		multMock := NewMulticallMock()
		balanceChecker := New(&ethrpccliMock, nil, multMock, 1, batchSize)
		errChan := make(chan error)

		balancesChan := balanceChecker.Producer(triggers, errChan)

		for balances := range balancesChan {
			if len(balances) == 2 {
				assert.Equal(t, "10000", balances[0].Balance)
				assert.Equal(t, "20000", balances[1].Balance)
			}

			if len(balances) == 1 {
				assert.Equal(t, "30000", balances[0].Balance)
			}
		}

		assert.Equal(t, 2, multMock.CallInvokedCount)
		assert.Len(t, errChan, 0)
	})

	t.Run("Producer - batch size 1", func(t *testing.T) {
		batchSize := 1
		multMock := NewMulticallMock()
		balanceChecker := New(&ethrpccliMock, nil, multMock, 1, batchSize)
		errChan := make(chan error)

		balancesChan := balanceChecker.Producer(triggers, errChan)
		for balances := range balancesChan {
			assert.Len(t, balances, 1)
			if balances[0].ContractAddress == "0x0d8775f648430679a709e98d2b0cb6250d2887ef" {
				assert.Equal(t, "10000", balances[0].Balance)
			}

			if balances[0].ContractAddress == "0x80fB784B7eD66730e8b1DBd9820aFD29931aab03" {
				assert.Equal(t, "20000", balances[0].Balance)
			}

			if balances[0].ContractAddress == "0xcb97e65f07da24d46bcdd078ebebd7c6e6e3d750" {
				assert.Equal(t, "30000", balances[0].Balance)
			}
		}
		assert.Len(t, errChan, 0)
		assert.Equal(t, 3, multMock.CallInvokedCount)
	})

	t.Run("Producer - error from multicall", func(t *testing.T) {

		multicallMock := &multicallMock{
			CallFn: func(calls multicall.ViewCalls, lastBlockHex string) (*multicall.Result, error) {
				assert.Equal(t, "blockhex", lastBlockHex)
				return nil, fmt.Errorf("failed to execute multicall request")
			},
		}

		balanceChecker := New(&ethrpccliMock, nil, multicallMock, 1, 1)
		errChan := make(chan error)

		balancesChan := balanceChecker.Producer(triggers, errChan)
		assert.Len(t, balancesChan, 0)

		err := <-errChan
		assert.EqualError(t, err, "failed to execute multicall request")
	})

}

func TestConsumer(t *testing.T) {
	t.Run("Success balances saved in db", func(t *testing.T) {
		dbMock := sqliteMock{
			InsertBalancesFn: func(balances []Balance) error {
				assert.Len(t, balances, 2)
				assert.Equal(t, "10000", balances[0].Balance)
				assert.Equal(t, "20000", balances[1].Balance)
				return nil
			},
		}

		batchSize := 2
		nrConsumers := 1
		balanceChecker := New(nil, &dbMock, nil, nrConsumers, batchSize)
		balancesChan := make(chan []Balance)
		errChan := make(chan error)
		done := make(chan bool)

		go balanceChecker.Consumer(balancesChan, errChan, done)

		balances := []Balance{
			{
				TriggerName:     "BAT",
				UserAddress:     "0x0000000000000000000000000000000000000000",
				ContractAddress: "0x0d8775f648430679a709e98d2b0cb6250d2887ef",
				Balance:         "10000",
			},
			{
				TriggerName:     "ETHLAND",
				UserAddress:     "0x0000000000000000000000000000000000000000",
				ContractAddress: "0x80fB784B7eD66730e8b1DBd9820aFD29931aab03",
				Balance:         "20000",
			},
		}

		balancesChan <- balances
		close(balancesChan)

		<-done
		assert.Len(t, errChan, 0)
	})

	t.Run("Failed to save balances to db", func(t *testing.T) {
		dbMock := sqliteMock{
			InsertBalancesFn: func(balances []Balance) error {
				assert.Len(t, balances, 2)
				assert.Equal(t, "10000", balances[0].Balance)
				assert.Equal(t, "20000", balances[1].Balance)
				return fmt.Errorf("failed to save to db")
			},
		}

		batchSize := 2
		nrConsumers := 1
		balanceChecker := New(nil, &dbMock, nil, nrConsumers, batchSize)
		balancesChan := make(chan []Balance)
		errChan := make(chan error)
		done := make(chan bool)

		go balanceChecker.Consumer(balancesChan, errChan, done)

		balances := []Balance{
			{
				TriggerName:     "BAT",
				UserAddress:     "0x0000000000000000000000000000000000000000",
				ContractAddress: "0x0d8775f648430679a709e98d2b0cb6250d2887ef",
				Balance:         "10000",
			},
			{
				TriggerName:     "ETHLAND",
				UserAddress:     "0x0000000000000000000000000000000000000000",
				ContractAddress: "0x80fB784B7eD66730e8b1DBd9820aFD29931aab03",
				Balance:         "20000",
			},
		}

		balancesChan <- balances
		close(balancesChan)

		err := <-errChan
		assert.EqualError(t, err, "failed to save to db")
	})
}
