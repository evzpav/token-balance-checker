package sqlite

import (
	"fmt"
	"token-balance-checker/balancechecker"

	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

const (
	tokenbalancesTableName string = "token_balances"
)

type SqliteDB struct {
	db        *gorm.DB
	tableName string
}

func InitDb() (*SqliteDB, error) {
	db, err := gorm.Open(sqlite.Open("token_balances.db"), &gorm.Config{})
	if err != nil {
		return nil, fmt.Errorf("failed to connect to db: %v", err)
	}

	return &SqliteDB{
		db:        db,
		tableName: tokenbalancesTableName,
	}, nil

}

func (sdb *SqliteDB) CreateTokenBalancesTable() error {
	return sdb.db.Exec(`CREATE TABLE IF NOT EXISTS token_balances (
		id INTEGER PRIMARY KEY AUTOINCREMENT,	
		trigger_name TEXT,
		balance TEXT,
		contract_address TEXT,
		user_address TEXT,
		UNIQUE(contract_address, user_address)
		)`).Error

}

func (sdb *SqliteDB) InsertBalances(balances []balancechecker.Balance) error {
	return sdb.db.Table(sdb.tableName).Clauses(clause.OnConflict{
		Columns: []clause.Column{
			{Name: "contract_address"},
			{Name: "user_address"},
		},
		DoUpdates: clause.AssignmentColumns([]string{"balance"}),
	}).Create(&balances).Error
}

func (sdb *SqliteDB) GetAllBalances() ([]balancechecker.Balance, error) {
	var balances []balancechecker.Balance
	err := sdb.db.Table(sdb.tableName).Find(&balances).Error
	if err != nil {
		return nil, err
	}

	return balances, nil
}
