package main

import (
	"log"
	"token-balance-checker/balancechecker"
	"token-balance-checker/ethrpcclient"
	"token-balance-checker/multicaller"
	"token-balance-checker/sqlite"

	"github.com/HAL-xyz/web3-multicall-go/multicall"
	"github.com/ethereum/go-ethereum/common"
)

const (
	nodeURL          string = "https://mainnet.infura.io/v3/17ed7fe26d014e5b9be7dfff5368c69d"
	triggersFileName string = "triggers.json"
	nrOfConsumers    int    = 3
	callsBatchSize   int    = 3
)

func main() {

	sqLiteDB, err := sqlite.InitDb()
	if err != nil {
		log.Fatalf("failed to create db: %v", err)
	}

	if err := sqLiteDB.CreateTokenBalancesTable(); err != nil {
		log.Fatalf("failed to create db table: %v", err)
	}

	cli := ethrpcclient.New(nodeURL)

	mc, err := multicaller.New(&cli, multicall.MainnetAddress)
	if err != nil {
		log.Fatalf("failed to instantiate multicaller: %v", err)
	}

	bc := balancechecker.New(&cli, sqLiteDB, mc, nrOfConsumers, callsBatchSize)

	var triggersInput []*balancechecker.Trigger
	if err := common.LoadJSON(triggersFileName, &triggersInput); err != nil {
		log.Fatalf("failed to read file: %v", err)
	}

	if err := bc.CheckBalances(triggersInput); err != nil {
		log.Fatalf("failed to check balances: %v", err)
	}

}
