package ethrpcclient

import (
	"fmt"

	"github.com/HAL-xyz/ethrpc"
)

type EthRPCClient struct {
	cli *ethrpc.EthRPC
}

func New(url string) EthRPCClient {
	return EthRPCClient{cli: ethrpc.New(url)}
}

func (erc EthRPCClient) MakeEthRpcCall(cntAddress, data string, blockNumber int) (string, error) {
	params := ethrpc.T{
		To:   cntAddress,
		From: "0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee",
		Data: data,
	}
	hexBlockNo := fmt.Sprintf("0x%x", blockNumber)

	return erc.cli.EthCall(params, hexBlockNo)
}

func (erc EthRPCClient) LastBlockHex() (string, error) {
	var lastBlockNo, err = erc.cli.EthBlockNumber()
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("%x", lastBlockNo), nil
}
