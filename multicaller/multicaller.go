package multicaller

import (
	"token-balance-checker/ethrpcclient"

	"github.com/HAL-xyz/web3-multicall-go/multicall"
)

type multicaller struct {
	mc multicall.Multicall
}

func New(ethrpccli *ethrpcclient.EthRPCClient, contractAddress string) (*multicaller, error) {
	mc, err := multicall.New(ethrpccli, multicall.ContractAddress(contractAddress))
	if err != nil {
		return nil, err
	}

	return &multicaller{mc: mc}, nil
}

func (mcr *multicaller) Call(calls multicall.ViewCalls, lastBlockHex string) (*multicall.Result, error) {
	return mcr.mc.Call(calls, lastBlockHex)

}
