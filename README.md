# Token Balance Checker

It checks smart contracts balances for multiple contract addresses based on owner address.

First it parses a list of triggers from a json file and get unique ones by contractAddress+userAddress as key to avoid repeated calls.
It uses the multicall lib [multicall](https://github.com/HAL-xyz/web3-multicall-go) make `balanceOf` calls for each trigger based on contract address and owner address (user address).  
A multicall is a smart contract deployed on Ethereum that batches together different calls.
Only `balanceOf` calls are allowed for this version.

Implemented using producer-consumer pattern.
- Producer makes batches of 3 call and executes the 3-calls batch, in 3 go-routines at a time.
- Consumer receives the balances and stores all balances at once, when all calls are finished. Balances are saved to a [sqlite](https://sqlite.org/) database in table called `token_balances`.

## Run project
#### Prerequisite:
- [Golang](http://golang.org/)(>11.0)

```bash
    go run main.go
```
It will print in the terminal the results:
```
Token balances checker is done
Balances saved in db:
Balance: {ID:1 TriggerName:Bytom UserAddress:0x0000000000000000000000000000000000000000 ContractAddress:0xcb97e65f07da24d46bcdd078ebebd7c6e6e3d750 Balance:128613051588836533}
Balance: {ID:2 TriggerName:BAT UserAddress:0x0000000000000000000000000000000000000000 ContractAddress:0x0d8775f648430679a709e98d2b0cb6250d2887ef Balance:532542704197680694082798}
Balance: {ID:3 TriggerName:DGD UserAddress:0x0000000000000000000000000000000000000000 ContractAddress:0xe0b7927c4af23765cb51314a0e0521a9645f0e2a Balance:1919017613796950}
Balance: {ID:4 TriggerName:Cryptaur UserAddress:0x0000000000000000000000000000000000000000 ContractAddress:0x88d50b466be55222019d71f9e8fae17f5f45fca1 Balance:1891447856964150000}
Balance: {ID:5 TriggerName:ETHLAND UserAddress:0x0000000000000000000000000000000000000000 ContractAddress:0x80fB784B7eD66730e8b1DBd9820aFD29931aab03 Balance:2500719022157750149139073}
Balance: {ID:6 TriggerName:BC UserAddress:0x0000000000000000000000000000000000000000 ContractAddress:0x21ab6c9fac80c59d401b37cb43f81ea9dde7fe34 Balance:57853466624377366}
```

## Run all unit tests
```bash
    go test -v balancechecker/*.go
```